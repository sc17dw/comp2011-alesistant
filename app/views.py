from flask import render_template, flash, redirect, abort, session
from app import app, db, models
from .forms import Alesistant, SignInForm, SignUpForm
import datetime
import os

@app.route('/')
def openSite():
    #TODO: Check if logged in, return accordingly
    if (session.get('Logstat') is not None):
        return redirect('/pubs')
    else:
        return redirect('/signin')

@app.route('/map')
def mapView():
    if (session.get('Logstat') is None):
        return redirect('/')
    pubs = models.AlesistantList.query.filter_by(user=session.get('Logstat')).all()
    if (len(pubs) == 0):
        flash("Map unavailable, no pubs added")
        return redirect('/add')
    return render_template("map.html",
        title="Pub Map",
        pubs=pubs)

@app.route('/pubs')
def showAllPubs():
    if (session.get('Logstat') is None):
        return redirect('/')
    pubs = models.AlesistantList.query.filter_by(user=session.get('Logstat')).all()
    return render_template("pubs.html",
        title="Pubs",
        pubs=pubs)

@app.route('/pubs/complete')
def showCompletePubs():
    if (session.get('Logstat') is None):
        return redirect('/')
    pubs = models.AlesistantList.query.filter_by(complete=True,user=session.get('Logstat')).all()
    return render_template("pubs.html",
        title="Completed Pubs",
        pubs=pubs)

@app.route('/pubs/view/<id>')
def view(id):
    if (session.get('Logstat') is None):
        return redirect('/')
    pub = models.AlesistantList.query.filter_by(id=id, user=session.get('Logstat')).first()
    return render_template("view.html",
        title=pub.name,
        pub=pub)

@app.route('/pubs/incomplete')
def showIncompletePubs():
    if (session.get('Logstat') is None):
        return redirect('/')
    pubs = models.AlesistantList.query.filter_by(complete=False,user=session.get('Logstat')).all()
    return render_template("pubs.html",
        title="Incomlete Pubs",
        pubs=pubs)

@app.route('/add', methods=['GET', 'POST'])
def pub():
    if (session.get('Logstat') is None):
        return redirect('/')
    form = Alesistant()
    if form.validate_on_submit():
        flash('%s added to the list.'%(form.name.data))
        pub = models.AlesistantList( name=form.name.data, postcode=form.postcode.data, complete=form.complete.data, user=session['Logstat'])
        db.session.add(pub)
        db.session.commit()
    return render_template('add.html',
        title='Add Pub',
        form=form)

@app.route('/pubs/complete/<id>')
def complete(id):
    if (session.get('Logstat') is None):
        return redirect('/')
    pubs = models.AlesistantList.query.filter_by(complete=True).all()
    updateID = models.AlesistantList.query.filter_by(id=id).first()
    updateID.complete = True
    db.session.commit()
    return redirect('/pubs')

@app.route('/signin', methods=['GET', 'POST'])
def signin():
    if (session.get('Logstat') is not None):
        return redirect('/')
    form = SignInForm()
    if form.validate_on_submit():
        email = str(form.email.data).lower()
        password = str(form.password.data)
        userDB = models.UsersList.query.filter_by(email=email).first()
        if (userDB == None):
            flash("Username or password is incorrect")
            return redirect('/signup')
        if (str(userDB.email) == email and str(userDB.password) == password):
            flash("Welcome back, %s"%(userDB.name))
            session['Logstat'] = email
            return redirect('/')
        else:
            flash("Username or password is incorrect")
            return redirect('/signup')
    return render_template('signin.html',
        title='Sign-In',
        form=form)

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if (session.get('Logstat') is not None):
        return redirect('/')
    form = SignUpForm()
    if form.validate_on_submit():
        if (form.password.data != form.confPass.data):
            flash("Passwords do not match")
            return redirect('/signup')
        user = models.UsersList(name=form.name.data, email=form.email.data.lower(), password=form.password.data)
        db.session.add(user)
        db.session.commit()
        flash("%s's profile has been created."%(form.name.data))
        return redirect('/signin')
    return render_template('signup.html',
        title='Sign-Up',
        form=form)

@app.route('/signout')
def signout():
    session.pop('Logstat', None)
    return redirect('/')
