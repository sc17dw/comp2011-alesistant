from flask_wtf import Form
from wtforms import TextField
from wtforms import BooleanField
from wtforms import DateField
from wtforms.validators import DataRequired

class Alesistant(Form):
    name = TextField('title', validators=[DataRequired()])
    postcode = TextField('postcode')
    complete = BooleanField('complete')

class SignInForm(Form):
    email = TextField('email', validators=[DataRequired()])
    password = TextField('password', validators=[DataRequired()])

class SignUpForm(Form):
    name = TextField('name', validators=[DataRequired()])
    email = TextField('email', validators=[DataRequired()])
    password = TextField('password', validators=[DataRequired()])
    confPass = TextField('confirm', validators=[DataRequired()])
