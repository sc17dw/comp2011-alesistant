from app import db

class AlesistantList(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    complete = db.Column(db.Boolean)
    name = db.Column(db.String(50), index=True)
    postcode = db.Column(db.String(500), index=True)
    user = db.Column(db.String(500), index=True)

class UsersList(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), index=True)
    email = db.Column(db.String(50), index=True)
    password = db.Column(db.String(500), index=True)

    def __init__(self, name, email, password):
        self.name =name
        self.email=email
        self.password=password
